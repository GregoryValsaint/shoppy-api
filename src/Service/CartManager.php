<?php


namespace App\Service;


use App\Entity\Order;
use App\Entity\User;
use App\Repository\OrderRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Security;

class CartManager
{
  /** @var Security */
  private $security;
  /** @var OrderRepository */
  private $repository;
  /** @var ObjectManager */
  private $manager;

  /**
   * CartManager constructor.
   * @param Security $security
   * @param OrderRepository $repository
   * @param ObjectManager $manager
   */
  public function __construct(Security $security, OrderRepository $repository, EntityManagerInterface $manager)
  {
    $this->security = $security;
    $this->repository = $repository;
    $this->manager = $manager;
  }

  public function getCart()
  {
    /** @var User $user */
    $user = $this->security->getUser();
    if (!$user) {
      return null;
    }
    $cart = $this->repository->getUserCart($user);
    if (!$cart) {
      $cart = new Order();
      $cart->setUser($user);
      $this->manager->persist($cart);
      $this->manager->flush();
    }
    return $cart;
  }

  public function updateStocks(Order $cart)
  {
    $outOfStocks = [];
    foreach ($cart->getOrderLines() as $line){
      if($line->getProduct()->getStock() < $line->getQuantity()){
        $outOfStocks[]=$line->getProduct();
      }
    }
    if(sizeof($outOfStocks) > 0){
      return $outOfStocks;
    }
    foreach ($cart->getOrderLines() as $line){
      $stock = $line->getProduct()->getStock() - $line->getQuantity();
      $line->getProduct()->setStock($stock);
    }
    $this->manager->flush();
    return [];
  }
}
